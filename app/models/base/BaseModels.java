package models.base;

import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.*;
import org.joda.time.*;

import play.db.jpa.*;

import com.thoughtworks.xstream.annotations.*;

@MappedSuperclass
public abstract class BaseModels extends GenericModel {
    
    public static final Integer DISABLE = 0;
    public static final Integer ENABLE = 1;
    
    @XStreamAlias("ID番号")
    @Id
    @GeneratedValue
    protected Long id;
    
    @XStreamOmitField
    @Version
    private Long version;
    
    @XStreamAlias("作成日時")
    @Column(nullable = false)
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    protected DateTime createDate;
    
    @XStreamAlias("更新日時")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    protected DateTime modifyDate;
    
    @XStreamAlias("無効フラグ")
    @Column(nullable = false)
    protected int disable = DISABLE;
    
    public Long getId() {
        return id;
    }
    
    public Long id() {
        return getId();
    }
    
    @Override
    public Object _key() {
        return getId();
    }
    
    public DateTime createDate() {
        return createDate;
    }
    
    public DateTime modifyDate() {
        return modifyDate;
    }
    
    public boolean isDisable() {
        return disable == DISABLE
                ? false
                : true;
    }
    
    public void enable() {
        this.disable = DISABLE;
        this.save();
    }
    
    public void disable() {
        this.disable = ENABLE;
        this.save();
    }
    
    @PrePersist
    protected void prePersist() {
        createDate = new DateTime();
    }
    
    /**
     * 保存時にエンティティ保存仕様を満たすか確認する
     */
    @Override
    public <T extends JPABase> T save() {
        isSatisfied();
        _save();
        return (T) this;
    }
    
    /**
     * エンティティが仕様を満たしているかどうか
     */
    public abstract boolean isSatisfied();
    
    /** リストに変換 */
    protected static <E> List<E> varlist(final Object list) {
        return (List<E>) list;
    }
    
    /** 変更不可リストに変換 */
    protected static <E> List<E> vallist(final Object list) {
        return Collections.<E> unmodifiableList((List<E>) list);
    }
    
}
