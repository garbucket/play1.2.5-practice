package models.taskapp.project;

import javax.persistence.*;

import models.base.*;
import models.taskapp.project.vo.*;

/**
 * プロジェクトエンティティ
 */
@Entity(name = "project")
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"projectName"})})
public class Project extends BaseModels {
    
    /** プロジェクト名 */
    @Embedded
    private final ProjectName projectName;
    
    /** 完了可否 */
    @Column(name = "done")
    private boolean isDone = false;
    
    public Project(final ProjectName projectName) {
        this.projectName = projectName;
    }
    
    @Override
    public boolean isSatisfied() {
        //TODO nullチェック
        
        //重複チャック
        final Project other = ProjectRepo.findBy(this.projectName);
        if (other != null && !other.equals(this)) {
            throw new RuntimeException(this.projectName.value());
        }
        return false;
    }
    
    public ProjectName projectName() {
        return this.projectName;
    }
    
    public boolean isDone() {
        return this.isDone;
    }
    
    public Project toggleDone() {
        this.isDone = isDone
                ? false
                : true;
        return this;
    }
    
    @Override
    public Project delete() {
        super.disable();
        return this;
    }
    
}
