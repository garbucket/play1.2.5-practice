package models.taskapp.project;

import models.taskapp.project.vo.*;

/**
 * プロジェクトRepo
 */
public class ProjectRepo {
    
    public static Project findBy(final ProjectName projectName) {
        return Project.find("projectName =?", projectName).first();
    }
    
}
