package models.taskapp.project.vo;

import javax.persistence.*;

/**
 * プロジェクト名
 */
@Embeddable
public class ProjectName {
    
    @Column(name = "projectName", nullable = false, length = 200)
    private final String value;
    
    public ProjectName(final String value) {
        this.value = value;
    }
    
    public String value() {
        return this.value;
    }
    
    @Override
    public String toString() {
        return this.value;
    }
}
