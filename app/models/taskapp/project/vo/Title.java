package models.taskapp.project.vo;

import javax.persistence.*;

@Embeddable
public class Title {
    
    @Column(name = "title", nullable = false)
    private final String value;
    
    public Title(final String value) {
        this.value = value;
    }
    
    public String value() {
        return this.value;
    }
    
    @Override
    public String toString() {
        return this.value;
    }
}
