package models.taskapp.project;

import static org.hamcrest.CoreMatchers.*;
import models.taskapp.*;
import models.taskapp.project.*;
import models.taskapp.project.vo.*;

import org.junit.*;

import play.test.*;

public class ProjectTest extends UnitTest {
    
    @Before
    public void before() {
        Fixtures.deleteDatabase();
    }
    
    @Test
    public void test() {
        final long count = Project.count();
        new Project(new ProjectName("プロジェクト")).save();
        assertThat(Project.count(), is(count + 1));
    }
    
    @Test
    public void testToggleDone() {
        final Project project = new Project(new ProjectName("プロジェクト")).save();
        assertThat(project.isDone(), is(false));
        
        project.toggleDone().save();
        assertThat(project.isDone(), is(true));
    }
}
